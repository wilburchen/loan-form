export type Entity = "Loan" | "Borrower";

export interface StringConditions {
  regex: string;
};

// assumption that minValue will always be smaller than maxValue
// potentially add runtime check
export interface MoneyConditions {
  minValue: number;
  maxValue: number;
}
interface BaseConfigField {
  entity: Entity;
  display: string;
  field: string;
}

export interface StringField extends BaseConfigField {
  type: "string";
  conditions: StringConditions;
};

export interface MoneyField extends BaseConfigField {
  type: "money";
  conditions: MoneyConditions;
};

export interface DateField extends BaseConfigField {
  type: "date";
}

export type ConfigField =
  StringField |
  MoneyField |
  DateField;