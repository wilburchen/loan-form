import { Entity } from './configTypes';

export interface LoanData {
  loanAmount: number | null;
  loanType: string;
  downPaymentAmount: number | null;
};

export type FieldStatus = 'DEFAULT' | 'ERROR' | 'COMPLETE';

// technically could have crazy numbers e.g. month: 100 but data should be validated prior to this
export interface BirthDate {
  month: number;
  day: number;
  year: number;
};

export interface BorrowerData {
  firstName: string;
  lastName: string;
  birthDate: BirthDate | null;
}

export interface FormData {
  Loan?: LoanData;
  Borrower?: BorrowerData;
};

export interface HandleChangeProps {
  value: string | number | BirthDate | null;
  entity: Entity;
  localState: FormData;
  field: string;
  setLocalState: Function;
};

export interface UpdateStoreStateProps {
  entity: Entity;
  field: string;
  value: string | number | BirthDate;
  storeState: FormData;
  setStoreState: Function;
};