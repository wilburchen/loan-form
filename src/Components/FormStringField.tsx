import { useState } from 'react';
import TextField from '@mui/material/TextField';
import FormHelperText from './FormHelperText';
import { StringField } from '../configTypes';
import { FormData, FieldStatus } from '../inputTypes';
import { 
  handleChange, 
  isValidStringInput, 
  updateStoreState,
  getDefaultStringStatus,
  getStringHelperText,
} from '../formUtils';
import get from 'lodash/get';

interface FormStringFieldProps {
  formField: StringField;
  localState: FormData;
  setLocalState: Function;
  storeState: FormData;
  setStoreState: Function;
};

function FormStringField({
  formField,
  localState,
  setLocalState,
  storeState,
  setStoreState,
}: FormStringFieldProps) {
  const { 
    field,
    entity,
    display,
    conditions,
  } = formField;

  const value = get(localState, [entity, field], '');
  const [fieldStatus, setFieldStatus] = useState<FieldStatus>(getDefaultStringStatus(value, conditions));

  // maybe generalize this into a util function
  const handleBlur = (e: any) => {
    const input = e.target.value;
    if (input === '') {
      setFieldStatus('DEFAULT');
    } else if (isValidStringInput(input, conditions)) {
      setFieldStatus('COMPLETE');
      updateStoreState({
        entity,
        field,
        value: input,
        storeState,
        setStoreState
      })
    } else {
      setFieldStatus('ERROR');
    }
  };


  return (
    <>
      <TextField
        id="outlined-basic"
        label={display}
        onChange={(e) => {
          if (fieldStatus === 'COMPLETE') {
            setFieldStatus('DEFAULT');
          }
          handleChange({
            value: e.target.value,
            entity,
            localState,
            field,
            setLocalState,
          });
        }}
        onBlur={handleBlur}
        value={value}
        error={fieldStatus === 'ERROR'}
        margin='normal'
        fullWidth
        size='small'
        InputProps={{
          "aria-label": `String Input Field ${display}`
        }}
      />
      <FormHelperText
        text={getStringHelperText(fieldStatus, display)}
        status={fieldStatus}
      />
    </>
  );
}

export default FormStringField;
